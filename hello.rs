/*
fn main() {
    println!("KTHX BY");
}
*/

pub struct Data {
    data: Vec<int>
}
pub trait Sortable {
    fn sort(&self) -> Self;
}
impl Sortable for Data {
    fn sort(&self) -> Data {
	fn quick_sort( data :&Vec<int> ) -> Vec<int>
	{
	    match data.len()  {
		0 => Vec::new(),
		1 => vec!(*data.get(0)),
		2 => {
		    let a = *data.get(0);
		    let b = *data.get(1);
		    match a < b {
			true => vec!(a,b),
			_    => vec!(b,a)
		    }
		}
		_ => {
		    let pivot = *data.get(data.len()/2);
		    let (front, back) = data.partitioned(|&n| n< pivot);
		    let (middle, after) = back.partitioned(|&n| n == pivot);
		    quick_sort(&front) + middle + quick_sort(&after)
		}
		
	    }
	}
	return Data{
	    data: quick_sort( &self.data )
	};
    }
}

#[cfg(test)]
mod test {
    use super::Data;
    use super::Sortable;

    #[test]
    fn testing_testing() {
	assert!( true );
    }
    #[test]
    fn empty_array() {
	let lst: Data = Data{ data: Vec::new() };
	let res: Data = Data{ data: Vec::new() };

    	assert_eq!(res.data, lst.sort().data);
    }

    #[test]
    fn single_element() {
	let lst: Data = Data{ data: vec!(1)};
	let res: Data = Data{ data: vec!(1)};

    	assert_eq!(res.data, lst.sort().data);
    }

    #[test]
    fn two_elements_in_order() {
	let lst: Data = Data{ data: vec!(1,2)};
	let res: Data = Data{ data: vec!(1,2)};

    	assert_eq!(res.data, lst.sort().data);
    }

    #[test]
    fn two_elements_out_of_order() {
	let lst: Data = Data{ data: vec!(2,1)};
	let res: Data = Data{ data: vec!(1,2)};

    	assert!(res.data == lst.sort().data);
    }

    #[test]
    fn three_elements_in_order() {
	let lst: Data = Data{ data: vec!(1,2,3)};
	let res: Data = Data{ data: vec!(1,2,3)};

    	assert!(res.data == lst.sort().data);
    }

    #[test]
    fn three_elements_first_two_out_of_order() {
	let lst: Data = Data{ data: vec!(2,1,3)};
	let res: Data = Data{ data: vec!(1,2,3)};

    	assert!(res.data == lst.sort().data);
    }
    #[test]
    fn three_elements_revers_order() {
	let lst: Data = Data{ data: vec!(3,2,1)};
	let res: Data = Data{ data: vec!(1,2,3)};

    	assert!(res.data == lst.sort().data);
    }
    #[test]
    fn three_elements_second_two_out_of_order() {
	let lst: Data = Data{ data: vec!(1,3,2)};
	let res: Data = Data{ data: vec!(1,2,3)};

    	assert!(res.data == lst.sort().data);
    }
    #[test]
    fn elements_out_of_order_with_dups() {
	let lst: Data = Data{ data: vec!(1,3,2,2,1,3,3,1,2)};
	let res: Data = Data{ data: vec!(1,1,1,2,2,2,3,3,3)};

    	assert!(res.data == lst.sort().data);
    }
/*
*/
}
